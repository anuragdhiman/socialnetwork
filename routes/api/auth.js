const express = require('express');
const router = express.Router();
const auth = require('../../middleware/auth');
const User = require('../../models/User');
const { check, validationResult } = require('express-validator'); // user to validate the payload we send....
const jwt = require('jsonwebtoken');
const config = require('config');
const bcrypt = require('bcryptjs');


// @route GET api/auth
// desc test route
// @access Public


router.get('/', auth, async(req,res) => {
   try {
     const user = await User.findById(req.user.id).select('-password');
      res.json(user);
   } catch(err) {
     console.error(err.message);
     res.send(500).send('Server Error');
   }
  });





// @route POST api/auth
// desc Authenticate user & get token...
// @access Public


router.post('/', [
  check('email', 'Please enter a valid email').isEmail(),
  check('password', 'Password is required').exists()
], async (req, res) => { // here we are using the asynch await.....
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    // console.log("here i am getting",errors)
    return res.status(400).json({ errors: errors.array() })
  }

  const { email, password } = req.body;

  try {
    // See if the user exists.....
    let user = await User.findOne({ email });

    console.log("user",user);
    if (!user) {
      return res.status(400).json({ errors: [{ msg: 'Invalid Credentials....' }] });
    }
    console.log("jljljljlkj",password,user.password);
    const isMatch = await bcrypt.compare(password,user.password);
    if(!isMatch) {
      return res
      .status(400)
      .json({errors: [{msg: 'Invalid Credentials....'}]})
    }   
   
    const payload = {
      user: {
        id: user.id
      }
    }

    jwt.sign(payload, config.get('jwtSecret'),
      { expiresIn: 36000 }, (err, token) => {
        if (err) throw err;
        res.json({ token });
      }
    );
    
  } catch (err) {
    console.log(err.message);
    res.status(500).send('Server error');
  }

});


module.exports = router;

