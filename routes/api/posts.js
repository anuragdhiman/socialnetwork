const express = require('express');
const router = express.Router();
const { check, validationResult } = require('express-validator'); // user to validate the payload we send....
const User = require('../../models/User'); // This is the schema which we have made for the user registration.....
const gravatar = require('gravatar');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const Post = require('../../models/Post');
const Profile = require('../../models/Profile');
const auth = require('../../middleware/auth');



// @route POST api/post
// desc test route
// @access Private


router.post('/', [
  auth,
  [
    check('text', 'Text is required').not().isEmpty()
  ]
], async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }

  try {


    const user = await User.findById(req.user.id).select('-password');
    const newPost = new Post({
      text: req.body.text,
      name: user.name,
      avatar: user.avatar,
      user: req.user.id

    })

    const post = await newPost.save();
    res.json(post);

  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }
}
);

// @route GET api/post
// desc test route
// @access Private

router.get('/', auth, async (req, res) => {
  try {
    const posts = await Post.find().sort({ date: -1 });
    res.json(posts);
  } catch (err) {
    console.error(err.message)
    res.status(500).send('Server Error');
  }
})


// @route GET api/post/:id
// desc  Get post by ID
// @access Private

router.get('/:id', auth, async (req, res) => {
  try {
    const post = await Post.findById(req.params.id);
    if (!post) {
      return res.status(404).json({ msg: "Post is not found" });
    }
    res.json(post);
  } catch (err) {
    console.error(err.message)
    if (err.kind === "ObjectId") {
      return res.status(404).json({ msg: "Post is not found" });
    }
    res.status(500).send('Server Error');
  }
})



// @route DELETE api/post/:id
// desc  Delete post by ID
// @access Private

router.delete('/:id', auth, async (req, res) => {
  try {
    const post = await Post.findById(req.params.id);
    // Check if post found or not....... 
    if (!post) {
      return res.status(404).json({ msg: "Post is not found" });
    }
    // Check user

    if (post.user.toString() !== req.user.id) {
      return res.status(401).json({ msg: 'User not authorized' });
    }

    await post.remove();

    res.json({ msg: 'Post Removed' });
  } catch (err) {
    console.error(err.message)
    if (err.kind === "ObjectId") {
      return res.status(404).json({ msg: "Post is not found" });
    }
    res.status(500).send('Server Error');
  }
})


// @route DELETE api/post/like/:id
// desc  Like a post 
// @access Private

router.put('/like/:id', auth, async (req, res) => {
  try {
    const post = await Post.findById(req.params.id);
    // check if the post is already been liked 
    if (post.likes.filter(like => like.user.toString() === req.user.id).lenght > 0) {
      return res.status(400).json({ msg: 'Post already liked' });
    }

    post.likes.unshift({ user: req.user.id });
    await post.save();

  } catch (err) {
    console.error(err.message)
    res.status(500).send('Server Error');
  }
})


// @route DELETE api/post/unlike/:id
// desc  unlike a post 
// @access Private

router.put('/unlike/:id', auth, async (req, res) => {
  try {
    const post = await Post.findById(req.params.id);
    // check if the post is already been liked 
    if (post.likes.filter(like => like.user.toString() === req.user.id).lenght === 0) {
      return res.status(400).json({ msg: 'Post has not been liked yet' });
    }

    // Get remove index
    const removeIndex = post.likes.map(like => like.user.toString()).indexOf(req.user.id);

    post.likes.splice(removeIndex, 1);
    await post.save();
    res.json(post.likes);

  } catch (err) {
    console.error(err.message)
    res.status(500).send('Server Error');
  }
})


// @route POST api/post/comment/:id
// desc test comment on the post
// @access Private


router.post('/comment/:id', [
  auth,
  [
    check('text', 'Text is required').not().isEmpty()
  ]
], async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }

  try {
    const user = await User.findById(req.user.id).select('-password');

    const post = await Post.findById(req.params.id);
    const newComment = {
      text: req.body.text,
      name: user.name,
      avatar: user.avatar,
      user: req.user.id

    }
    post.comments.unshift(newComment)

    await post.save();
    res.json(post.comment);

  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }
}
);



// @route Delete api/post/comment/:id/:comment_id
// desc n delete comment on the post
// @access Private

router.delete('/comment/:id/:comment_id', auth, async (req, res) => {
  try {
    const post = await User.findById(req.params.id);

    //Pull out the comment
    const comment = post.comments.find(comment => comment.id === req.params.comment_id);
      
    // Make sure the comment is exist.......

  if(!comment) {
    return res.status(404).json({ msg: 'Comment does not exists'});
  }
  if(comment.user.toString() !== req.user.id) {
    return res.status(401).json({msg: 'User not authorized'});
  }

  // Get remove index

  const removeIndex = post.comments.map(comment.user.toString()).indexOf(req.user.id);
  post.comments.splice(removeIndex,1);
  await post.save()
  res.json(post.comments);
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }

})

module.exports = router;