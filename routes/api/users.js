// This route is used to register the new user.....

const express = require('express');
const router = express.Router();

const { check, validationResult } = require('express-validator'); // user to validate the payload we send....
const User = require('../../models/User'); // This is the schema which we have made for the user registration.....
const gravatar = require('gravatar');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const config = require('config');

// @route POST api/users
// desc register user
// @access Public


router.post('/', [
  check('name', 'Name is required')
    .not()
    .isEmpty(),
  check('email', 'Please enter a valid email').isEmail(),
  check('password', 'Please enter a password with 6 or more characters').isLength({ min: 6 })
], async (req, res) => { // here we are using the asynch await.....
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    // console.log("here i am getting",errors)
    return res.status(400).json({ errors: errors.array() })
  }

  const { name, email, password } = req.body;

  try {
    // See if the user exists.....
    let user = await User.findOne({ email });
    if (user) {
      return res.status(400).json({ errors: [{ msg: 'User is already exists' }] });
    }

    const avatar = gravatar.url(email, { // use the dp that we have in our mail and save in the database
      s: '200',
      r: 'pg',
      d: 'mm'
    })

    user = new User({
      name,
      email,
      avatar,
      password
    })

    const salt = await bcrypt.genSalt(10) // this salt is use to generate the hash.....

    user.password = await bcrypt.hash(password, salt); // this is hashed our password.....
    await user.save();

    const payload = {
      user: {
        id: user.id
      }
    }

    jwt.sign(payload, config.get('jwtSecret'),
      { expiresIn: 36000 }, (err, token) => {
        if (err) throw err;
        res.json({ token });
      }
    );
    
  } catch (err) {
      console.log(err.message);
    res.status(500).send('Server error in route user');
  }

});

module.exports = router; 
