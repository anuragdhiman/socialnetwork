import  { combineReducers } from 'redux';
import alert from './alert';
import auth from './auth';
import profile from './profile';

// combine the all the states for different actions here and available for the components via mapStateToProps....
export default combineReducers({
   alert,auth,profile
});