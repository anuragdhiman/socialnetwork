import { SET_ALERT, REMOVE_ALERT } from './types';
import * as uuid from 'uuid'


export const setAlert = (msg, alertType, timeout = 1000) => dispatch => {
  console.log("alert is called here")
  const id = uuid.v4();
  dispatch({
    type: SET_ALERT,
    payload: { msg, alertType, id }
  });
  setTimeout(() => dispatch({ type: REMOVE_ALERT, payload: id }), timeout);
};

export default setAlert;