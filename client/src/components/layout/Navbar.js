import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { logout } from '../../actions/auth';
import { connect } from 'react-redux';


const Navbar = ({ auth: { isAuthenticated, loading }, logout }) => {

    const authLinks = (
        <ul>
            <li><Link to="/dashboard"><i className = "fas fa-user" /> {' '}
             <span className = 'hide-sm'>  
            </span>Dashboard</Link>
            </li>
            <li>
                <Link onClick={logout} to="/dashboard">
                    <i className='fas fa-sign-out-alt' /> {' '}
                    <span className='hide-sm'> Logout </span>
                </Link>
            </li>
        </ul>
    );

    const guestLinks = (
        <ul>
            <li><Link to="/dashboard">Developers</Link></li>
            <li><Link to="/register">Register</Link></li>
            <li><Link to="/login">Login</Link></li>
        </ul>
    )

    return (
        <nav className="navbar bg-dark">
            <h1>
              <Link to="/"><i className="fas fa-code"></i> DevConnector</Link>
            </h1>
    {!loading && (<Fragment> { isAuthenticated ? authLinks: guestLinks}</Fragment>)}
        </nav>
    )
}


Navbar.propTypes = {
    logout: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired
}
const mapStateToProps = state => ({
    auth: state.auth
})


export default connect(mapStateToProps, { logout })(Navbar);

/*
1. Here we are implenting navbar...
2. So to check the auth state we are accessing here.....
3. Also we need logout action here.....
*/