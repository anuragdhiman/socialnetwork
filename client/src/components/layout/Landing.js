import React from 'react';
import { Link, Redirect } from 'react-router-dom';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';

// Here we are using the funtional components along with the hooks......
const Landing = ({isAuthenticated}) => {
    if(isAuthenticated) {
        return <Redirect to = '/dashboard'/>
    }
    return (
        <section className="landing">
            <div className="dark-overlay">
                <div className="landing-inner">
                    <h1 className="x-large">Developer Connector</h1>
                    <p className="lead"> Create a developer profile/portfolio, share posts and get help from other developers </p>
                    <div className="buttons">
                        <Link to="/register" className="btn btn-primary">Sign Up</Link>
                        <Link to="/login" className="btn btn-light">Login</Link>
                    </div>
                </div>
            </div>
        </section>
    )
}

// Type checking for props that we are passing....... 
Landing.prototype = {
    isAuthenticated: PropTypes.bool.isRequired
}

// Use to bring the entire state of the application
const mapStateToProps = state => ({
   isAuthenticated: state.auth.isAuthenticated 
})

// Connect is use for the mapping/connecting of the state to the component....
export default connect(mapStateToProps)(Landing);


//----------------------------------------------------- End of Code --------------------------------------------------------------------
/* 
1. Here we are not calling any action.
2. All we need here is Authentication that is why we are just accessing the state...........
*/